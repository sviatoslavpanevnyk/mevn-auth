import Vue from 'vue'
import Router from 'vue-router'
import router from './router'
import './styles/main.css'
import Main from './pages/Main.vue'

Vue.use(Router)

const app = new Vue({
  el: '#app',
  router,
  render: createElement => createElement(Main)
})